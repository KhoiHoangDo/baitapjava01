package buoi01;

import java.util.Scanner;

public class Bai02 {
    private static Scanner scanner = new Scanner(System.in);
    public static void doiCoSo(int n,int base){
        if(n>=base) doiCoSo(n / base, base);
        if(n % base>9) System.out.printf("%c",n%base+55);
        else
            System.out.print((n % base));
    }

    public static void main(String[] args) {
        System.out.println("Nhập n:");
        int n= scanner.nextInt();
        System.out.println("Nhập vào cơ số cần chuyển sang b: ");
        int b= scanner.nextInt();
        System.out.println("Số " +n+ " chuyển sang cơ số " +b+ " thành: ");
        doiCoSo(n,b);
    }
}