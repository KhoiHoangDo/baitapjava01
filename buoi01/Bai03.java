package buoi01;

import java.util.Scanner;

public class Bai03 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int n, soDu, tong = 0;
        System.out.println("Nhập vào số bất kỳ: ");
        n = scanner.nextInt();

        while (n > 0) {
            soDu = n % 10;
            n = n / 10;
            tong += soDu;
        }
        System.out.println("Tổng các chữ số = " + tong);
    }
}