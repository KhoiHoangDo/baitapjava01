package buoi01;

import java.util.Scanner;

public class Bai01_UCBC {
    private static Scanner scanner = new Scanner(System.in);
    public static int USCLN(int a, int b){
        while(a!= b){
            if(a>b) a= a-b;
            else b= b-a;
        }
        return (a);
    }
    public static void main(String[] args)
    {
        System.out.print("Nhập số a: ");
        int a = scanner.nextInt();
        System.out.print("Nhập số b: ");
        int b = scanner.nextInt();
        // tính USCLN của a và b
        System.out.println("USCLN của " + a + " và " + b
                + " là: " + USCLN(a, b));
        // tính BSCNN của a và b
        System.out.println("BSCNN của " + a + " và " + b
                + " là: " + ((a*b)/USCLN(a,b)));
    }
}