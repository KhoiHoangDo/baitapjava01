package buoi01;

import java.util.Scanner;

public class Bai05 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Nhập n = ");
        int n = scanner.nextInt();
        System.out.printf("Tất cả các số nguyên tố nhỏ hơn %d là: \n", n);
        if (n >= 2) {
            System.out.print(2);
        }
        for (int i = 3; i < n; i+=2) {
            if (checkSNT(i)) {
                System.out.print(" " + i);
            }
        }
    }
    public static boolean checkSNT(int n) {
        // số nguyên n < 2 không phải là số nguyên tố
        if (n < 2) {
            return false;
        }
        // check số nguyên tố khi n >= 2
        int canBacHai = (int) Math.sqrt(n);
        for (int i = 2; i <= canBacHai; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}