package buoi01;

import java.util.Scanner;

public class Bai06 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Nhập n = ");
        int n = scanner.nextInt();
        System.out.printf("%d số nguyên tố đầu tiên là: \n", n);
        int dem = 0; // đếm số số nguyên tố
        int i = 2;   // tìm số nguyên tố bắt dầu từ số 2
        while (dem < n) {
            if (checkSNT(i)) {
                System.out.print(i + " ");
                dem++;
            }
            i++;
        }
    }

    public static boolean checkSNT(int n) {
        // số nguyên n < 2 không phải là số nguyên tố
        if (n < 2) {
            return false;
        }
        // check số nguyên tố khi n >= 2
        int canBacHai = (int) Math.sqrt(n);
        for (int i = 2; i <= canBacHai; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}